# Commission fees calculator

A commission fee calculator app written in Node.js using test driven development.

# Run application

```bash
$ yarn start PATH_TO_FILE
```

# Run in watch mode

```bash
$ yarn start:dev PATH_TO_FILE
```
