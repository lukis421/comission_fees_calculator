import { itemSchema } from './schema'
import { cloneDeep, merge, omit } from 'lodash'

describe('Validation schema', () => {
    const correctValue: { [key: string]: any } = {
        date: "2016-01-05",
        user_id: 1,
        user_type: "natural",
        type: "cash_in",
        operation: {
            amount: 200,
            currency: "EUR"
        }
    }

    describe('Required fields', () => {
        it('Throws an error if date field is absent', async () => {
            const withoutDate = omit(correctValue, 'date')

            await expect(itemSchema.validateAsync(withoutDate)).rejects.toThrow()
        })

        it('Throws an error if user_id field is absent', async () => {
            const withoutUserId = omit(correctValue, 'user_id')

            await expect(itemSchema.validateAsync(withoutUserId)).rejects.toThrow()
        })

        it('Throws an error if user_type field is absent', async () => {
            const withoutUserType = omit(correctValue, 'user_type')

            await expect(itemSchema.validateAsync(withoutUserType)).rejects.toThrow()
        })

        it('Throws an error if type field is absent', async () => {
            const withoutType = omit(correctValue, 'type')

            await expect(itemSchema.validateAsync(withoutType)).rejects.toThrow()
        })

        it('Throws an error if operation field is absent', async () => {
            const withoutOperation = omit(correctValue, 'operation')

            await expect(itemSchema.validateAsync(withoutOperation)).rejects.toThrow()
        })

        it('Throws an error if amount field is absent', async () => {
            const withoutAmount = merge(
                    omit(correctValue, 'operation'),
                    omit(correctValue.operation, 'amount')
            )

            await expect(itemSchema.validateAsync(withoutAmount)).rejects.toThrow()
        })

        it('Throws an error if currency field is absent', async () => {
            const withoutCurrency = merge(
                    omit(correctValue, 'operation'),
                    omit(correctValue.operation, 'currency')
            )

            await expect(itemSchema.validateAsync(withoutCurrency)).rejects.toThrow()
        })

        it('Doesn\'t throw an error if all required fields are present', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()
        })
    })

    describe('date field', () => {
        it('Throws an error if date field is null', async () => {
            const nullValue = cloneDeep(correctValue)
            nullValue.date = null
            await expect(itemSchema.validateAsync(nullValue)).rejects.toThrow()
        })

        it('Throws an error if date field is undefined', async () => {
            const undefinedValue = cloneDeep(correctValue)
            undefinedValue.date = undefined
            await expect(itemSchema.validateAsync(undefinedValue)).rejects.toThrow()
        })

        it('Throws an error if date field is an object', async () => {
            const objectValue = cloneDeep(correctValue)
            objectValue.date = {}
            await expect(itemSchema.validateAsync(objectValue)).rejects.toThrow()
        })

        it('Throws an error if date field is an array', async () => {
            const arrayValue = cloneDeep(correctValue)
            arrayValue.date = []
            await expect(itemSchema.validateAsync(arrayValue)).rejects.toThrow()
        })

        it('Throws an error if date field is a boolean', async () => {
            const booleanValue = cloneDeep(correctValue)
            booleanValue.date = true
            await expect(itemSchema.validateAsync(booleanValue)).rejects.toThrow()
        })

        it('Throws an error if date field is not a valid date', async () => {
            const notValidDate = cloneDeep(correctValue)
            notValidDate.date = '2021-48-34'
            await expect(itemSchema.validateAsync(notValidDate)).rejects.toThrow()
        })

        it ('Doesn\'t throw an error if date field is a valid date', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()
        })
    })

    describe('user_id field', () => {
        it('Throws an error if user_id field is null', async () => {
            const nullValue = cloneDeep(correctValue)
            nullValue.user_id = null
            await expect(itemSchema.validateAsync(nullValue)).rejects.toThrow()
        })

        it('Throws an error if user_id field is undefined', async () => {
            const undefinedValue = cloneDeep(correctValue)
            undefinedValue.user_id = undefined
            await expect(itemSchema.validateAsync(undefinedValue)).rejects.toThrow()
        })

        it('Throws an error if user_id field is an object', async () => {
            const objectValue = cloneDeep(correctValue)
            objectValue.user_id = {}
            await expect(itemSchema.validateAsync(objectValue)).rejects.toThrow()
        })

        it('Throws an error if user_id field is an array', async () => {
            const arrayValue = cloneDeep(correctValue)
            arrayValue.user_id = []
            await expect(itemSchema.validateAsync(arrayValue)).rejects.toThrow()
        })

        it('Throws an error if user_id field is a boolean', async () => {
            const booleanValue = cloneDeep(correctValue)
            booleanValue.user_id = true
            await expect(itemSchema.validateAsync(booleanValue)).rejects.toThrow()
        })

        it('Throws an error if user_id field is a string', async () => {
            const stringValue = cloneDeep(correctValue)
            stringValue.user_id = '576a4b2c-f87c-4f41-bb1d-8419ae7d9666'
            await expect(itemSchema.validateAsync(stringValue)).rejects.toThrow()
        })

        it('Throws an error if user_id field is less than 1', async () => {
            const lessThan1 = cloneDeep(correctValue)
            lessThan1.user_id = 0
            await expect(itemSchema.validateAsync(lessThan1)).rejects.toThrow()
        })

        it ('Doesn\'t throw an error if user_id field is an integer larger than 0', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()
        })
    })

    describe('user_type field', () => {
        it('Throws an error if user_type field is null', async () => {
            const nullValue = cloneDeep(correctValue)
            nullValue.user_type = null
            await expect(itemSchema.validateAsync(nullValue)).rejects.toThrow()
        })

        it('Throws an error if user_type field is undefined', async () => {
            const undefinedValue = cloneDeep(correctValue)
            undefinedValue.user_type = undefined
            await expect(itemSchema.validateAsync(undefinedValue)).rejects.toThrow()
        })

        it('Throws an error if user_type field is an object', async () => {
            const objectValue = cloneDeep(correctValue)
            objectValue.user_type = {}
            await expect(itemSchema.validateAsync(objectValue)).rejects.toThrow()
        })

        it('Throws an error if user_type field is an array', async () => {
            const arrayValue = cloneDeep(correctValue)
            arrayValue.user_type = []
            await expect(itemSchema.validateAsync(arrayValue)).rejects.toThrow()
        })

        it('Throws an error if user_type field is a boolean', async () => {
            const booleanValue = cloneDeep(correctValue)
            booleanValue.user_type = true
            await expect(itemSchema.validateAsync(booleanValue)).rejects.toThrow()
        })

        it('Throws an error if user_type field is a number', async () => {
            const numberValue = cloneDeep(correctValue)
            numberValue.user_type = 123
            await expect(itemSchema.validateAsync(numberValue)).rejects.toThrow()
        })

        it('Throws an error if user_type is not "natural" or "juridical"', async () => {
            const stringValue = cloneDeep(correctValue)
            stringValue.user_type = 'default'
            await expect(itemSchema.validateAsync(stringValue)).rejects.toThrow()
        })

        it ('Doesn\'t throw an error if user_type field is either "natural" or "juridical"', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()

            const juridicalValue = cloneDeep(correctValue)
            juridicalValue.user_type = 'juridical'

            await expect(itemSchema.validateAsync(juridicalValue)).resolves.not.toThrow()
        })
    })

    describe('type field', () => {
        it('Throws an error if type field is null', async () => {
            const nullValue = cloneDeep(correctValue)
            nullValue.type = null
            await expect(itemSchema.validateAsync(nullValue)).rejects.toThrow()
        })

        it('Throws an error if type field is undefined', async () => {
            const undefinedValue = cloneDeep(correctValue)
            undefinedValue.type = undefined
            await expect(itemSchema.validateAsync(undefinedValue)).rejects.toThrow()
        })

        it('Throws an error if type field is an object', async () => {
            const objectValue = cloneDeep(correctValue)
            objectValue.type = {}
            await expect(itemSchema.validateAsync(objectValue)).rejects.toThrow()
        })

        it('Throws an error if type field is an array', async () => {
            const arrayValue = cloneDeep(correctValue)
            arrayValue.type = []
            await expect(itemSchema.validateAsync(arrayValue)).rejects.toThrow()
        })

        it('Throws an error if type field is a boolean', async () => {
            const booleanValue = cloneDeep(correctValue)
            booleanValue.type = true
            await expect(itemSchema.validateAsync(booleanValue)).rejects.toThrow()
        })

        it('Throws an error if type field is a number', async () => {
            const numberValue = cloneDeep(correctValue)
            numberValue.type = 123
            await expect(itemSchema.validateAsync(numberValue)).rejects.toThrow()
        })

        it('Throws an error if type is not "cash_in" or "cash_out"', async () => {
            const stringValue = cloneDeep(correctValue)
            stringValue.type = 'withdraw'
            await expect(itemSchema.validateAsync(stringValue)).rejects.toThrow()
        })

        it('Doesn\'t throw an error if type field is either "cash_in" or "cash_out"', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()

            const cashOutValue = cloneDeep(correctValue)
            cashOutValue.type = 'cash_out'

            await expect(itemSchema.validateAsync(cashOutValue)).resolves.not.toThrow()
        })
    })

    describe('operation field', () => {
        it('Throws an error if operation field is null', async () => {
            const nullValue = cloneDeep(correctValue)
            nullValue.operation = null
            await expect(itemSchema.validateAsync(nullValue)).rejects.toThrow()
        })

        it('Throws an error if operation field is undefined', async () => {
            const undefinedValue = cloneDeep(correctValue)
            undefinedValue.operation = undefined
            await expect(itemSchema.validateAsync(undefinedValue)).rejects.toThrow()
        })

        it('Throws an error if operation field is an array', async () => {
            const arrayValue = cloneDeep(correctValue)
            arrayValue.operation = []
            await expect(itemSchema.validateAsync(arrayValue)).rejects.toThrow()
        })

        it('Throws an error if operation field is a boolean', async () => {
            const booleanValue = cloneDeep(correctValue)
            booleanValue.operation = true
            await expect(itemSchema.validateAsync(booleanValue)).rejects.toThrow()
        })

        it('Throws an error if operation field is a number', async () => {
            const numberValue = cloneDeep(correctValue)
            numberValue.operation = 123
            await expect(itemSchema.validateAsync(numberValue)).rejects.toThrow()
        })

        it('Throws an error if operation field is an object with missing required properties: "amount" and "currency"', async () => {
            const withoutAmount = cloneDeep(correctValue)
            delete withoutAmount.operation.amount
            await expect(itemSchema.validateAsync(withoutAmount)).rejects.toThrow()

            const withoutCurrency = cloneDeep(correctValue)
            delete withoutCurrency.operation.currency
            await expect(itemSchema.validateAsync(withoutCurrency)).rejects.toThrow()
        })

        it('Doesn\'t throw an error if operation field is an object with required properties: "amount" and "currency', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()
        })
    })

    describe('amount field', () => {
        it('Throws an error if amount field is null', async () => {
            const nullValue = cloneDeep(correctValue)
            nullValue.operation.amount = null
            await expect(itemSchema.validateAsync(nullValue)).rejects.toThrow()
        })

        it('Throws an error if amount field is undefined', async () => {
            const undefinedValue = cloneDeep(correctValue)
            undefinedValue.operation.amount = undefined
            await expect(itemSchema.validateAsync(undefinedValue)).rejects.toThrow()
        })

        it('Throws an error if amount field is an object', async () => {
            const objectValue = cloneDeep(correctValue)
            objectValue.operation.amount = {}
            await expect(itemSchema.validateAsync(objectValue)).rejects.toThrow()
        })

        it('Throws an error if amount field is an array', async () => {
            const arrayValue = cloneDeep(correctValue)
            arrayValue.operation.amount = []
            await expect(itemSchema.validateAsync(arrayValue)).rejects.toThrow()
        })

        it('Throws an error if amount field is a boolean', async () => {
            const booleanValue = cloneDeep(correctValue)
            booleanValue.operation.amount = true
            await expect(itemSchema.validateAsync(booleanValue)).rejects.toThrow()
        })

        it('Throws an error if amount field is a string', async () => {
            const stringValue = cloneDeep(correctValue)
            stringValue.operation.amount = '10zł'
            await expect(itemSchema.validateAsync(stringValue)).rejects.toThrow()
        })

        it('Throws an error if amount field is a number less than 0', async () => {
            const lessThan0 = cloneDeep(correctValue)
            lessThan0.operation.amount = -10
            await expect(itemSchema.validateAsync(lessThan0)).rejects.toThrow()
        })

        it('Doesn\'t throw an error if amount field is a number more or equal to 0', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()

            const equal0 = cloneDeep(correctValue)
            equal0.operation.amount = 0

            await expect(itemSchema.validateAsync(equal0)).resolves.not.toThrow()
        })
    })

    describe('currency field', () => {
        it('Throws an error if currency field is null', async () => {
            const nullValue = cloneDeep(correctValue)
            nullValue.operation.currency = null
            await expect(itemSchema.validateAsync(nullValue)).rejects.toThrow()
        })

        it('Throws an error if currency field is undefined', async () => {
            const undefinedValue = cloneDeep(correctValue)
            undefinedValue.operation.currency = undefined
            await expect(itemSchema.validateAsync(undefinedValue)).rejects.toThrow()
        })

        it('Throws an error if currency field is an object', async () => {
            const objectValue = cloneDeep(correctValue)
            objectValue.operation.currency = {}
            await expect(itemSchema.validateAsync(objectValue)).rejects.toThrow()
        })

        it('Throws an error if currency field is an array', async () => {
            const arrayValue = cloneDeep(correctValue)
            arrayValue.operation.currency = []
            await expect(itemSchema.validateAsync(arrayValue)).rejects.toThrow()
        })

        it('Throws an error if currency field is a boolean', async () => {
            const booleanValue = cloneDeep(correctValue)
            booleanValue.operation.currency = true
            await expect(itemSchema.validateAsync(booleanValue)).rejects.toThrow()
        })

        it('Throws an error if currency field is a number', async () => {
            const numberValue = cloneDeep(correctValue)
            numberValue.operation.currency = 120
            await expect(itemSchema.validateAsync(numberValue)).rejects.toThrow()
        })

        it('Throws an error if currency field is not "EUR"', async () => {
            const stringValue = cloneDeep(correctValue)
            stringValue.operation.currency = 'PLN'
            await expect(itemSchema.validateAsync(stringValue)).rejects.toThrow()
        })

        it('Doesn\'t throw an error if currency field is "EUR"', async () => {
            await expect(itemSchema.validateAsync(correctValue)).resolves.not.toThrow()
        })
    })
})