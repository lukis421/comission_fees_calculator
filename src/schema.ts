import * as Joi from 'joi'
import { Currency, OperationType, UserType } from './constants'

export const itemSchema = Joi.object({
    date: Joi.date().required(),
    user_id: Joi.number().required().min(1),
    user_type: Joi.required().valid(...Object.values(UserType)),
    type: Joi.required().valid(...Object.values(OperationType)),
    operation: Joi.object({
        amount: Joi.number().required().min(0),
        currency: Joi.valid(...Object.values(Currency)).required()
    }).required()
})

export const fullSchema = Joi.array().items(itemSchema)