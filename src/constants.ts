export enum Currency {
    EUR = 'EUR'
}

export enum OperationType {
    CashIn = 'cash_in',
    CashOut = 'cash_out'
}

export enum UserType {
    Natural = 'natural',
    Juridical = 'juridical'
}